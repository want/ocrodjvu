Source: ocrodjvu
Section: text
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 djvulibre-bin,
 libxml2-utils,
 locales,
 python-all (>= 2.6.6-3~),
 python-all (>= 2.7.3-5~) | python-argparse (>= 1.2.1-2~),
 python-djvu (>= 0.3.9~),
 python-html5lib,
 python-lxml,
 python-nose,
 python-pil,
 python-pyicu (>= 1.0~)
Build-Conflicts:
 locales-all
Standards-Version: 4.4.1
Homepage: http://jwilk.net/software/ocrodjvu
Vcs-Git: https://salsa.debian.org/python-team/packages/ocrodjvu.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/ocrodjvu

Package: ocrodjvu
Architecture: all
Depends:
 djvulibre-bin,
 python (>= 2.7) | python-argparse,
 python-djvu (>= 0.3.9~),
 python-subprocess32,
 ${misc:Depends},
 ${python:Depends}
Recommends:
 python-html5lib,
 python-lxml,
 python-pyicu (>= 1.0~),
 tesseract-ocr
Suggests:
 cuneiform,
 gocr,
 ocrad
Description: tool to perform OCR on DjVu documents
 Ocrodjvu is a wrapper around the Optical Character Recognition (OCR)
 systems Cuneiform, Gocr, Ocrad, OCRopus and (standalone) Tesseract. It
 is designed for OCR on documents in DjVu format, which is especially
 suited for high-quality archiving of books.
 .
 After processing, the DjVu document embeds a text layer. Other programs can
 then be used to read the document, search it for specific terms, print it out,
 or use the information in the OCR layer as a way to improve the document's
 accessibility.
